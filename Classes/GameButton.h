//
//  GameButton.h
//  moleit-x
//
//  Created by Todd Perkins on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef moleit_x_GameButton_h
#define moleit_x_GameButton_h

#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,"paulos",__VA_ARGS__)

#include "cocos2d.h"
#include "Utils.h"

class GameButton : public cocos2d::CCSprite
{
    
    
    public:
    bool initWithText(const char * text, bool isBig, bool pressed);
    bool initWithImage(const char * imageName, bool pressed);
    static GameButton* buttonWithText(const char * text, bool isBig, bool pressed);
    static GameButton* buttonWithImage(const char *imageName, bool pressed);
};

#endif
