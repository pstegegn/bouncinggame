//
//  Utils.h
//  moleit-x
//
//  Created by Todd Perkins on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef Utils_h
#define Utils_h

#include "cocos2d.h"
#include "Game.h"
#include "HUD.h"
#include "AppDelegate.h"

#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,"paulos",__VA_ARGS__)
static char *FONTSYLE = (char *)"BALTAR.ttf";
static char *FONTNO = (char *)"neuropol.ttf";
using namespace cocos2d;

// The font size 24 is designed for small resolution, so we should change it to fit for current design resolution
//smallResource.size.width = 320
#define TITLE_FONT_SIZE  (cocos2d::CCEGLView::sharedOpenGLView()->getDesignResolutionSize().width / 320 * 24)

class Utils
{
public:
    static CCLayer* gameLayer();
    static CCLayer* hudLayer();
    static cocos2d::CCLayer* layerWithTag(int tag);
    static cocos2d::CCSize s();
    static CCSize size();
    static CCSize origin();
    static cocos2d::CCAnimate* getAnimationWithFrames(int from, int to);
    static void scaleSprite(cocos2d::CCSprite * sprite);
    static float getScale();
    static void setScale(float s);
    static float getArtScaleFactor();
    static void setArtScaleFactor(int s);
    static void beginSignIn();
    //static int getLag();
};

#endif
