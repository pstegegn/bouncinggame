/*
 * GroundSprite.h
 *
 *  Created on: Feb 20, 2014
 *      Author: paulos
 */

#ifndef GROUNDSPRITE_H_
#define GROUNDSPRITE_H_

#include "cocos2d.h"
#include "Block.h"
#include "CCParallaxNodeExtras.h"
#include "Box2D/Box2D.h"

USING_NS_CC;

class Ground : public CCSprite {

private:
	CCParallaxNodeExtras * _backgroundNode;
	//CCArray * _blocks;
	CCSprite* _block1;
	CCSprite* _block2;
	CCSprite* _block3;

    b2Body * GroundB2body;
	void initGround(void);
	CCSize _screenSize;

public:
	Ground(void);
	~Ground(void);
	static Ground * create();
	void move(float dt);
	void createB2Body(b2World*);
};



#endif /* GROUNDSPRITE_H_ */
