/*
 * MonsterTruck.h
 *
 *  Created on: Mar 20, 2014
 *      Author: paulos
 */

#ifndef MONSTERTRUCK_H_
#define MONSTERTRUCK_H_

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "time.h"

USING_NS_CC;

#define PTM_RATIO 32.0
#define BREAKLIMIT 0.3

class MonsterTruck : public CCNode
{
private:
  b2World *_world;
  b2Body *truckBody, *frontWheel, *rearWheel, *frontAxle, *rearAxle;
  b2PrismaticJoint *frontSpring, *rearSpring;
  b2RevoluteJoint *frontMotor, *rearMotor;
  long double time_0;
  long double time_1;
  struct timeval  tv;
	//160, 50, 16
	float w ; //16
	float h ; //5
	float prevFrontdt;
	float prevReardt;

	//float r = 16;  //1,6
	bool loss;
  CCSprite* truckSprite, *fWheelSprite, *rWheelSprite;

public:
  MonsterTruck();
  ~MonsterTruck();
  void createTruckWithWorld(b2World *, CCPoint);
  CCPoint position();
  void updateSprings();
  static MonsterTruck * create();
  void addTruckWithCoords(CCPoint pos);
  void jump();
  void move();
  CC_SYNTHESIZE(bool, _start, Start);
  CC_SYNTHESIZE(bool, isGameOver, IsGameOver);
};



#endif /* MONSTERTRUCK_H_ */
