#ifndef __MAINMENU_H__
#define __MAINMENU_H__

#include "cocos2d.h"
#include "GameOver.h"

using namespace cocos2d;

class MainMenu : public cocos2d::CCLayer {
    cocos2d::CCSize s;
   // GameOver *go;
public:
    virtual bool init();
    virtual void playGame(CCObject* pSender);
    virtual void mainMenu(CCObject* pSender);
    virtual void showLeaderboard(CCObject* pSender);
    virtual void rate(CCObject* pSender);

    static CCScene* scene();
    CREATE_FUNC(MainMenu);
}; // __MAINMENU_H__


#endif
