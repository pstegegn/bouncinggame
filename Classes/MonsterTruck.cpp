#include "MonsterTruck.h"
#include  "Utils.h"

MonsterTruck::MonsterTruck(){
	gettimeofday(&tv, NULL);
	time_0 = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000 ;
	_start = true;
	isGameOver = false;
	loss = true;
	w =(176.0f/768)*Utils::s().width;// 41.249996;//(176.0f/768)* Utils::size().width;//176;
	h = (55.0f/768)*Utils::s().width;//12.890624;//(55.0f/1184)* Utils::size().height; //
}

MonsterTruck::~MonsterTruck(){

}

MonsterTruck * MonsterTruck::create(){
	MonsterTruck * player = new MonsterTruck();
	if (player /*&& player->initWithFile("player.png")*/) {
		//player->initPlayer();
		player->autorelease();
		return player;
	}
	CC_SAFE_DELETE(player);
	return NULL;
}

void MonsterTruck::addTruckWithCoords(CCPoint pos)
{

	bool x = rand() % 2;
	truckSprite = CCSprite::create();
	if(x){
		//truckSprite->initWithFile("truck.png");
		truckSprite->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("truck.png"));
	}else{
		//truckSprite->initWithFile("lada.png");
		truckSprite->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("lada.png"));
	}
	truckSprite->setAnchorPoint(ccp(0.5,4.0/5));
	truckSprite->setPosition(pos);
	truckSprite->setScaleX(w/truckSprite->getContentSize().width);
	truckSprite->setScaleY(h/truckSprite->getContentSize().height);

	this->addChild(truckSprite, 1);
	//LOGD("truck size %f, %f", truckSprite->getContentSize().width, truckSprite->getContentSize().height );

  // Create the body of the Truck.
  // use two box to make it looks like a truck.
  {
    b2BodyDef bd;
    bd.type = b2_dynamicBody;
    bd.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
    //bd.angularDamping = 0.2;
    //bd.linearDamping = 0.2;
    //bd.fixedRotation = true;

    truckBody = _world->CreateBody(&bd);

    b2PolygonShape shape;

    // the main body
    shape.SetAsBox(w/2/PTM_RATIO,((30.0/50)*h)/2/PTM_RATIO,
    		b2Vec2(0.0, ((-25.0/50)*h)/PTM_RATIO), 0.0);

    b2FixtureDef fd;
    fd.shape = &shape;
    fd.density = 1;
    fd.friction = 0.3;
    fd.restitution = 0.0;
    fd.filter.groupIndex = -1;
    truckBody->CreateFixture(&fd);

    // a smaller box on the main body
    shape.SetAsBox(((30.0/160)*w)/PTM_RATIO, ((20.0/50)*h)/2/PTM_RATIO,
    		b2Vec2(((-30.0/160)*w)/2/PTM_RATIO, 0.0/PTM_RATIO), 0.0);
    truckBody->CreateFixture(&fd);

  }

  // we need two axle to join the truck body and the wheels together
  // and add two springs to join the truck body and axle together.
  {
    b2PrismaticJointDef pjd;
    pjd.lowerTranslation = 0.0;
    pjd.upperTranslation = 0.0;
    pjd.enableLimit = true;
    //pjd.enableMotor = true;

    b2BodyDef bd;
    bd.type = b2_dynamicBody;
    bd.allowSleep = false;

    b2PolygonShape shape;
    b2FixtureDef fd;
    fd.shape = &shape;
    fd.density = 1;
    fd.friction = 0.2;
    fd.restitution = 0.1;
    fd.filter.groupIndex = -1;

    // front axle and front spring
    bd.position.Set((pos.x+((45.0/160)*w))/PTM_RATIO,
    		(pos.y-((30.0/50)*h))/PTM_RATIO);

    frontAxle = _world->CreateBody(&bd);
    shape.SetAsBox(((10.0/160)*w)/2/PTM_RATIO,
                       ((30.0/50)*h)/2/PTM_RATIO,
                       b2Vec2(/*(-45.0)/PTM_RATIO, (-30.0)/PTM_RATIO)*/0,0),
                       0);
    frontAxle->CreateFixture(&fd);

    pjd.Initialize(truckBody, frontAxle, frontAxle->GetWorldCenter(),
                   b2Vec2(-cos(M_PI/2), sin(M_PI/2)));
    frontSpring = (b2PrismaticJoint *)_world->CreateJoint(&pjd);
    // rear axle and front spring
    bd.position.Set( (pos.x-((45.0/160)*w))/PTM_RATIO,
    		(pos.y-((30.0/50)*h))/PTM_RATIO);

    shape.SetAsBox(((10.0/160)*w)/2/PTM_RATIO,
                   ((30.0/50)*h)/2/PTM_RATIO,
                   b2Vec2(/*(-45.0)/PTM_RATIO, (-30.0)/PTM_RATIO)*/0,0),
                   0);
    rearAxle = _world->CreateBody(&bd);
    rearAxle->CreateFixture(&fd);

    pjd.Initialize(truckBody, rearAxle, rearAxle->GetWorldCenter(),
                   b2Vec2(cos(M_PI/2), sin(M_PI/2)));
    rearSpring = (b2PrismaticJoint *)_world->CreateJoint(&pjd);

  }

  // add two big wheels
  {
    float radius = ((32.0/160)*w)/2;
    b2BodyDef bd;
    bd.type = b2_dynamicBody;
    bd.angularDamping = -0.2;
    bd.linearDamping = 0.0;

    b2CircleShape shape;
    shape.m_radius = radius/PTM_RATIO;

    b2FixtureDef fd;
    fd.shape = &shape;
    fd.density = 1;
    fd.friction = 5;
    fd.restitution = 0.2;
    fd.filter.groupIndex = -1;

    // front wheel
    bd.position.Set(frontAxle->GetWorldCenter().x,
                    frontAxle->GetWorldCenter().y-((30.0/50)*h)/2/PTM_RATIO);
    frontWheel = _world->CreateBody(&bd);
    b2Fixture* fixture = frontWheel->CreateFixture(&fd);
    //fixture->SetUserData( (void*)4 );

    // front wheel
    bd.position.Set(rearAxle->GetWorldCenter().x,
                    rearAxle->GetWorldCenter().y-((30.0/50)*h)/2/PTM_RATIO);
    rearWheel = _world->CreateBody(&bd);
    fixture = rearWheel->CreateFixture(&fd);
   // fixture->SetUserData( (void*)4 );
  }

  { // add joints
    b2RevoluteJointDef rjd;
    rjd.enableMotor = true;

    rjd.Initialize(frontAxle, frontWheel, frontWheel->GetWorldCenter());
    frontMotor = (b2RevoluteJoint*)_world->CreateJoint(&rjd);
    rjd.Initialize(rearAxle, rearWheel, rearWheel->GetWorldCenter());
    rearMotor = (b2RevoluteJoint*)_world->CreateJoint(&rjd);
  }

  	  //wheel sprite
  fWheelSprite = CCSprite::create();
  //fWheelSprite->initWithFile("wheel.png");
  fWheelSprite->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("wheel.png"));
  fWheelSprite->setAnchorPoint(ccp(0.5,0.5));
  fWheelSprite->setPosition(CCPoint(frontWheel->GetPosition().x*PTM_RATIO,
		  frontWheel->GetPosition().y*PTM_RATIO));
  this->addChild(fWheelSprite, 1);

  rWheelSprite = CCSprite::create();
  //rWheelSprite->initWithFile("wheel.png");
  rWheelSprite->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("wheel.png"));
  rWheelSprite->setAnchorPoint(ccp(0.5,0.5));
  rWheelSprite->setPosition(CCPoint(rearWheel->GetPosition().x*PTM_RATIO,
		  rearWheel->GetPosition().y*PTM_RATIO));
  this->addChild(rWheelSprite, 1);

  prevFrontdt = sqrt(pow(frontWheel->GetPosition().x - (truckBody->GetPosition().x), 2) +
  			pow(frontWheel->GetPosition().y - (truckBody->GetPosition().y) , 2));
  prevReardt = sqrt(pow(rearWheel->GetPosition().x - (truckBody->GetPosition().x), 2) +
  			pow(rearWheel->GetPosition().y - (truckBody->GetPosition().y) , 2));

}

void MonsterTruck::createTruckWithWorld(b2World * world, CCPoint pos){
	  _world = world;
	  this->addTruckWithCoords(pos);
}

CCPoint MonsterTruck::position(){
	return ccp(truckBody->GetPosition().x*PTM_RATIO, truckBody->GetPosition().y*PTM_RATIO);
}

// update springs constantly
void MonsterTruck::updateSprings(){
/*	if (frontSpring != NULL && rearSpring != NULL)
	  {
	    frontSpring->SetMaxMotorForce(20 + abs(100 * pow(frontSpring->GetJointTranslation(), 2)));
	    frontSpring->SetMotorSpeed(-10 * pow(frontSpring->GetJointTranslation(), 1));

	    rearSpring->SetMaxMotorForce(20 + abs(100 * pow(rearSpring->GetJointTranslation(), 2)));
	    rearSpring->SetMotorSpeed(-10 * pow(rearSpring->GetJointTranslation(), 1));
	  }*/
}

void MonsterTruck::jump(){
	b2Vec2 vel = truckBody->GetLinearVelocity();
	vel.y = (29.5/1080)*Utils::size().height;//upwards - don't change x velocity
	truckBody->SetLinearVelocity( vel );
	truckBody->SetAngularVelocity( 1);
	//_angleCount += 1;
}

void MonsterTruck::move(){
	b2Vec2 vel = truckBody->GetLinearVelocity();
	truckBody->ApplyForce( truckBody->GetMass()* 3 *_world->GetGravity(), truckBody->GetWorldCenter() );
	frontWheel->ApplyForce( frontWheel->GetMass()* 9 *_world->GetGravity(), frontWheel->GetWorldCenter() );
	rearWheel->ApplyForce( rearWheel->GetMass()* 9 *_world->GetGravity(), rearWheel->GetWorldCenter() );
	frontAxle->ApplyForce( frontAxle->GetMass()* 9 *_world->GetGravity(), frontAxle->GetWorldCenter() );
	rearAxle->ApplyForce( rearAxle->GetMass()* 9 *_world->GetGravity(), rearAxle->GetWorldCenter() );
	//frontWheel->SetAngularVelocity( -1 );

	float angle = -1*CC_RADIANS_TO_DEGREES(truckBody->GetAngle());

/*	if(truckBody->GetAngularVelocity() > 0){
		truckBody->SetAngularVelocity( -1 );
	}*/
	gettimeofday(&tv, NULL);
	time_1 = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000 ;

	if(_start){
		if(	(time_1 - time_0) > 500){
			truckBody->SetAngularVelocity( 15);
			time_0 = time_1;
		}
	}

	if(!isGameOver){
		if(truckBody->GetPosition().x > (Utils::origin().width + Utils::size().width * 0.2f) / PTM_RATIO){
			vel.x= -1;//upwards - don't change x velocity
			truckBody->SetLinearVelocity( vel );
		}
		if(truckBody->GetPosition().x < (Utils::origin().width + Utils::size().width * 0.2f) / PTM_RATIO){
			vel.x = 1;//upwards - don't change x velocity
			truckBody->SetLinearVelocity( vel );
		}
	}else if(loss){
		vel.x= 5;//upwards - don't change x velocity
		truckBody->SetLinearVelocity( vel );
		loss = false;
	}

	truckSprite->setPosition(CCPoint(truckBody->GetPosition().x * PTM_RATIO,
			truckBody->GetPosition().y * PTM_RATIO));
	truckSprite->setRotation (angle);
	fWheelSprite->setPosition(CCPoint(frontWheel->GetPosition().x*PTM_RATIO,
			frontWheel->GetPosition().y*PTM_RATIO));
	rWheelSprite->setPosition(CCPoint(rearWheel->GetPosition().x*PTM_RATIO,
			rearWheel->GetPosition().y*PTM_RATIO));

	float frontdt = sqrt(pow(frontWheel->GetPosition().x - (truckBody->GetPosition().x), 2) +
			pow(frontWheel->GetPosition().y - (truckBody->GetPosition().y) , 2));
	float reardt = sqrt(pow(rearWheel->GetPosition().x - (truckBody->GetPosition().x), 2) +
	  			pow(rearWheel->GetPosition().y - (truckBody->GetPosition().y) , 2));
	//check for broken wheel
	//LOGD(" %f", dt -prevdt);
	if( sqrt(pow((frontdt - prevFrontdt),2)) > BREAKLIMIT ){
		LOGD(" %f", frontdt -prevFrontdt);
		frontSpring->EnableLimit(false);
	}
	//LOGD(" %f", reardt -prevReardt);
	if(sqrt(pow((reardt - prevReardt),2)) > BREAKLIMIT){
		LOGD(" %f", reardt -prevReardt);
		rearSpring->EnableLimit(false);
	}
	prevFrontdt = frontdt;
	prevReardt = reardt;

}

