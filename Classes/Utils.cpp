//
//  Utils.cpp
//  moleit-x
//
//  Created by Todd Perkins on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#include "Utils.h"
#include "Constants.h"
#include <iostream>
#include <string>
#include <sstream>
#include "platform/android/jni/JniHelper.h"

using namespace cocos2d;

static float scale = 1.0f;
static int artScaleFactor = 1.0;

//static int _lag = 9;
#define PTM_RATIO 32.0

CCLayer* Utils::gameLayer()
{
    return Utils::layerWithTag(TAG_GAME_LAYER);
}
CCLayer* Utils::hudLayer()
{
    return Utils::layerWithTag(TAG_HUD);
}
CCLayer* Utils::layerWithTag(int tag)
{
    CCScene *sc = CCDirector::sharedDirector()->getRunningScene();
    if (sc->getTag() == TAG_GAME_SCENE) {
        CCLayer *layer = (CCLayer *)sc->getChildByTag(tag);
        return layer;
    }
    return NULL;
}

CCSize Utils::s()
{
    return CCDirector::sharedDirector()->getWinSize();
}

CCSize Utils::size(){
	return CCDirector::sharedDirector()->getVisibleSize();
}

CCSize Utils::origin(){
	return CCDirector::sharedDirector()->getVisibleOrigin();
}

cocos2d::CCAnimate* Utils::getAnimationWithFrames(int from, int to)
{
	CCArray* frames = CCArray::create();
	for (int i = from; i <= to; i++) {
	     CCString *str = CCString::createWithFormat("a%04d.png", i);
	     frames->addObject(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str->getCString()));
	}
	CCAnimation *animation = CCAnimation::createWithSpriteFrames(frames, 1.0f/24.0f);
	CCAnimate *a = CCAnimate::create(animation);
	return a;
}

void Utils::scaleSprite(CCSprite* sprite)
{
    float rX = Utils::s().width / sprite->getContentSize().width;
	float rY = Utils::s().height / sprite->getContentSize().height;
	sprite->setScaleX(rX);
	sprite->setScaleY(rY);
}

float Utils::getScale()
{
    return scale;
}

void Utils::setScale(float s)
{
    scale = s;
}

float Utils::getArtScaleFactor()
{
    return artScaleFactor;
}

void Utils::setArtScaleFactor(int s)
{
    artScaleFactor = s;
}

void Utils::beginSignIn(){
	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "com.paulos.bouncyride/BouncyRide"
				, "gameServicesSignIn"
				, "()V"))
		{
			t.env->CallStaticVoidMethod(t.classID,t.methodID);
			t.env->DeleteLocalRef(t.classID);
		}
}

/*int Utils::getLag(){
	return _lag--;
}*/
