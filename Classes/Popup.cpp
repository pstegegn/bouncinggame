//
//  Pause.cpp
//  moleit-x
//
//  Created by Todd Perkins on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "Popup.h"

#include "GameButton.h"
#include "MainMenu.h"
#include "Utils.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;
using namespace CocosDenshion;

bool Popup::initWithTitle(const char *title)
{
    if (!CCSprite::init()) {
        return false;
    }   
    cocos2d::CCSize s = CCDirector::sharedDirector()->getWinSize();
    
    CCSprite *m = CCSprite::create();//CCSprite::createWithSpriteFrameName("menu.png");
    //m->initWithFile("menu.png");
    m->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("menu.png"));
    m->setScale((Utils::size().width* 0.8) / m->getContentSize().width);

    m->setPosition(ccp(Utils::size().width/2 , Utils::size().height/3));
    //m->setScale(1);
    this->addChild(m,1);
    
    CCLabelTTF *titleLabel = CCLabelTTF::create(title, FONTSYLE, TITLE_FONT_SIZE	);
    titleLabel->setPosition(ccp(s.width/2,s.height/3 + (m->getContentSize().height/2 * m->getScale()) - titleLabel->getContentSize().height *1));
    this->addChild(titleLabel,1);
    titleLabel->setColor(ccBLACK);

    CCLabelTTF *titleShadow = CCLabelTTF::create(title, FONTSYLE, TITLE_FONT_SIZE	);
        titleShadow->setPosition(ccp(titleLabel->getPosition().x - 0.007*Utils::size().width,
        							titleLabel->getPosition().y + 0.007*Utils::size().height));
        titleShadow->setColor(ccWHITE);
    	this->addChild(titleShadow,1);

    this->setVisible(false);
    return true;
}

Popup* Popup::popupWithTitle(const char *title)
{
    Popup *pop = new Popup();
    pop->initWithTitle(title);
    pop->autorelease();
    return pop;
}

void Popup::show(bool shouldShow)
{
	CCScene *g = CCDirector::sharedDirector()->getRunningScene();
    if (shouldShow) {
        g->pauseSchedulerAndActions();
    }
    else {
        g->resumeSchedulerAndActions();
    }
    for (int i = 0; i < g->getChildrenCount(); i++) {
        CCNode *n = (CCNode *)g->getChildren()->objectAtIndex(i);
        if (shouldShow) {
            n->pauseSchedulerAndActions();
        }
        else {
            n->resumeSchedulerAndActions();
        }
    }
    this->setVisible(shouldShow);
    /*if(shouldShow)
    {
    		SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
    }
    else
    {
        	SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    }
*/
}

void Popup::addButtonWithText(const char* text, CCObject*target, SEL_MenuHandler selector)
{

    CCMenuItemSprite *btn = CCMenuItemSprite::create(GameButton::buttonWithText(text, false, false),
    		GameButton::buttonWithText(text, false, true), target, selector);
    if (menu == NULL) {
        menu = CCMenu::create(btn,NULL);
        menu->setPosition(ccp(Utils::s().width/2, Utils::s().height/3));
        this->addChild(menu,1);
    }
    else {
        menu->addChild(btn, 1);
    }
    menu->alignItemsHorizontally();
    //menu->alignItemsHorizontallyWithPadding(btn->getContentSize().width/8);

}
void Popup::addButtonWithImage(const char* path, CCObject*target, SEL_MenuHandler selector)
{

    CCMenuItemSprite *btn = CCMenuItemSprite::create(GameButton::buttonWithImage(path, false),
    		GameButton::buttonWithImage(path, true), target, selector);
    if (menu == NULL) {
        menu = CCMenu::create(btn,NULL);
        menu->setPosition(ccp(Utils::s().width/2, Utils::s().height/5));
        this->addChild(menu,1);
    }
    else {
        menu->addChild(btn, 1);
    }
   // menu->alignItemsHorizontally();
    menu->alignItemsHorizontallyWithPadding(btn->getContentSize().width/4);

}




