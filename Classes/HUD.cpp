//
//  HUD.cpp
//  moleit-x
//
//  Created by Todd Perkins on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "HUD.h"
#include "Utils.h"
#include "SimpleAudioEngine.h"
#include "Game.h"
#include "MainMenu.h"
#include "GameOver.h"
#include "Constants.h"
#include "platform/android/jni/JniHelper.h"

using namespace cocos2d;
using namespace CocosDenshion;

bool HUD::init()
{
    if (!CCLayer::init()) {
        return false;        
    }
    score = 0;
    float padding = 10;
    
    scoreLabel = CCLabelTTF::create("0", FONTNO, TITLE_FONT_SIZE*1.5);
    scoreLabel->setAnchorPoint(ccp(0,1));
    scoreLabel->setScale(Utils::getScale());
    scoreLabel->setPosition(ccp(Utils::s().width/2, Utils::s().height - 2*padding));
    this->addChild(scoreLabel,1);

    return true;
}

void HUD::didScore()
{
	score++;
	scoreLabel->setString(CCString::createWithFormat("%d",score)->getCString());
}

void HUD::submitScore(){

	int hs = CCUserDefault::sharedUserDefault()->getIntegerForKey("no");

	JniMethodInfo t;
	if (JniHelper::getStaticMethodInfo(t, "com.paulos.bouncyride/BouncyRide"
			, "submitScore"
			, "(I)V"))
	{
		t.env->CallStaticVoidMethod(t.classID,t.methodID, hs);
		t.env->DeleteLocalRef(t.classID);
	}
}

void HUD::missedMole()
{

}

void HUD::onExit()
{

}

int HUD::getScore(){
	return score;
}
