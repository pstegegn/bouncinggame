#include "Block.h"
#include "Utils.h"

Block::~Block(){
	//LOGD("Block::~Block()1");
	//_world->DestroyBody(_blockB2body);
	_blockB2body = NULL;
	//LOGD("Block::~Block()3");
}

Block::Block () {
    //get screen size
	_screenSize = CCDirector::sharedDirector()->getWinSize();
	this->setVisible(false);
    _tileWidth = Utils::size().width / TILE_W_SIZE;
    _tileHeight = Utils::size().height / TILE_H_SIZE;
}

Block * Block::create () {
    Block * block = new Block();
    if (block ) {
    	block->initWithFile("box.png");
    	//block->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("box.png"));
    	block->autorelease();
		return block;
	}
	CC_SAFE_DELETE(block);
	return NULL;
}

void Block::initBlock(){
	this->setTextureRect(CCRectMake(0, 0, Utils::size().width, Utils::size().height*0.2));
}

void Block::setupBlock (int width, int height, float yPositionRatio, int type, b2World* world) {
	_type = type;
	_width = width * _tileWidth;
	_height = height * _tileHeight;

	this->setScaleX(_width/this->getContentSize().width);
	this->setScaleY(_height/this->getContentSize().height);
	this->setAnchorPoint(ccp(0,0));
	//this->setTextureRect(CCRectMake(0, 0, _width, _height));
	this->setPositionY(Utils::size().height * (yPositionRatio));

	_world = world;
	createB2Body();

	switch (type) {
		case kBlockGap:
			_blockB2body->SetActive(false);
			this->setVisible(false);
			return;
		case kBlock1:
			//this->setColor(ccc3(200, 200, 200));
			break;
		case kBlock2:
			//this->setColor(ccc3(150, 150, 150));
			break;
		case kBlock3:
			//this->setColor(ccc3(100, 100, 100));
			break;
		case kBlock4:
			//this->setColor(ccc3(50, 50, 50));
			break;
	}
	this->setVisible(true);
}

void Block::setupBlock () {
	//float rX = _screenSize.width / this->getContentSize().width;
	//float rY = _screenSize.height / this->getContentSize().height;
	//this->setAnchorPoint(ccp(0,0));
	//this->setPosition(ccp(0.0f, 0.0f));
	//this->setScaleX(rX);
	//this->setScaleY(rY*0.5);
    //this->setTextureRect(CCRectMake(0, 0, _screenSize.width, _screenSize.height));
    this->setVisible(true);
}

void Block::createB2Body(){
	//define body
	b2BodyDef polyDef;
	polyDef.type = b2_kinematicBody;
	polyDef.position.Set(_width  * 0.0f / PTM_RATIO, this->getPositionY()/ PTM_RATIO);
	_blockB2body = _world->CreateBody(&polyDef);

	//define shape
	b2PolygonShape polyShape;
	polyShape.m_centroid = b2Vec2(0.0f, 0.0f);
	polyShape.SetAsBox(_width/PTM_RATIO/2, _height/PTM_RATIO/2,
			 b2Vec2(_width/PTM_RATIO/2, _height/PTM_RATIO/2), 0);

	//define fixture
	b2FixtureDef fixtureDefpoly;
	fixtureDefpoly.shape = &polyShape;
	fixtureDefpoly.density = 1;
	fixtureDefpoly.restitution = 0.0;
	_blockB2body->CreateFixture(&fixtureDefpoly);

	polyShape.SetAsBox((this->_width-1)/PTM_RATIO/2, 0,
			b2Vec2(this->_width/PTM_RATIO/2, _height/PTM_RATIO), 0);

	fixtureDefpoly.shape = &polyShape;
	b2Fixture* fixture = _blockB2body->CreateFixture(&fixtureDefpoly);
	fixture->SetUserData( (void*)2 );
}

void Block::destroyB2Body(){
	_world->DestroyBody(_blockB2body);
}

