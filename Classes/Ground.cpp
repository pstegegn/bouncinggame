#include "Ground.h"
#include "CCParallaxNodeExtras.h"
#include "Utils.h"
#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,"paulos",__VA_ARGS__)
#define PTM_RATIO 32

Ground::~Ground(){

}

Ground::Ground(){

}

Ground* Ground::create(){
	Ground * ground = new Ground();
	if (ground && ground->initWithFile("blank.png")) {
		ground->initGround();
		ground->autorelease();
		return ground;
	}
	CC_SAFE_DELETE(ground);
	return NULL;
}

void Ground::initGround(){
	_screenSize = Utils::s();
	_backgroundNode = CCParallaxNodeExtras::node(); //1

	this->setTextureRect(CCRectMake(0, 0, _screenSize.width, _screenSize.height*0.2));
	this->setColor(ccc3(255,100,0));
	this->setAnchorPoint(ccp(0,0));
	//the ground should be at the fornt 1.
	this->addChild(_backgroundNode,1);

	_backgroundNode->setPosition(0,0);
	_backgroundNode->setAnchorPoint(ccp(0,0));
	//this->setScaleY(0.3);

	_block1 = CCSprite::create("tibeb.png");
	//_block1->setScaleX(_screenSize.width/ _block1->getContentSize().width);
	_block1->setScaleY((_screenSize.height*0.2)/ _block1->getContentSize().height);
	_block1->setVisible(true);
	//float scale = _screenSize.width/ _block1->getContentSize().width;

	_block2 = CCSprite::create("tibeb.png");
	//_block2->setScaleX(_screenSize.width/ _block2->getContentSize().width);
	_block2->setScaleY((_screenSize.height*0.2)/ _block2->getContentSize().height);
	_block2->setVisible(true);

	_block3 = CCSprite::create("tibeb.png");
	//_block3->setScaleX(_screenSize.width/ _block2->getContentSize().width);
	_block3->setScaleY((_screenSize.height*0.2)/ _block2->getContentSize().height);
	_block3->setVisible(true);

	CCPoint dustSpeed = ccp(0.15, 0.15);

	// 4) Add children to CCParallaxNode
	_backgroundNode->addChild(	_block1, 0, dustSpeed, ccp(
			_block1->getContentSize().width/2, _screenSize.height* 0.2/2) );


	_backgroundNode->addChild(_block2, 0, dustSpeed, ccp(
		_block1->getContentSize().width + _block2->getContentSize().width/2, _screenSize.height* 0.2/2));

	_backgroundNode->addChild(_block3, 0, dustSpeed, ccp(
			_block1->getContentSize().width + _block2->getContentSize().width +
			_block3->getContentSize().width/2, _screenSize.height* 0.2/2));

/*
	LOGD("block size: %f, %f", _block1->getContentSize().width, _block1->getContentSize().height);
	LOGD("block pos: %f, %f", _block1->getPosition().x, _block1->getPosition().y);
	LOGD("origin%f, %f", Utils::origin().width, Utils::origin().height);
	LOGD("backnode pos : %f %f", _backgroundNode->getPosition().x, _backgroundNode->getPosition().y);
	LOGD("this pos: %f %f", _block1->getPosition().x, _block1->getPosition().y);
	LOGD("visible: %f %f", Utils::size().width, Utils::size().height);
	LOGD("this: %f %f", this->getContentSize().width, this->getContentSize().height);*/
}

void Ground::move(float dt){
	CCPoint backgroundScrollVert = ccp(-1000, 0);
	_backgroundNode->setPosition(ccpAdd(_backgroundNode->getPosition(), ccpMult(backgroundScrollVert, dt)));

	CCArray *_blocks = CCArray::createWithCapacity(2);
	_blocks->addObject(_block1);
	_blocks->addObject(_block2);
	_blocks->addObject(_block3);

	for ( int ii = 0; ii <_blocks->count(); ii++ ) {
	    CCSprite * block = (CCSprite *)(_blocks->objectAtIndex(ii));
	    float xPosition = _backgroundNode->convertToWorldSpace(block->getPosition()).x;
	    float size = block->getContentSize().width;
	   // LOGD("Xpos, size: %f %f",xPosition, xPosition2);
	    if ( xPosition < -size) {
	        _backgroundNode->incrementOffset(ccp(block->getContentSize().width*3,0),block);
	    }
	}


}

void Ground::createB2Body(b2World* _world){
	//define body
	b2BodyDef polyDef;
	polyDef.type = b2_staticBody;
	polyDef.position.Set(Utils::origin().width / PTM_RATIO,
						 Utils::origin().height/ PTM_RATIO);
	GroundB2body = _world->CreateBody(&polyDef);

	//define shape
	b2PolygonShape polyShape;
	polyShape.m_centroid = b2Vec2(0.0f, 0.0f);
	polyShape.SetAsBox(this->getContentSize().width/PTM_RATIO,
			this->getContentSize().height/PTM_RATIO );

	//define fixture
	b2FixtureDef fixtureDefpoly;
	fixtureDefpoly.shape = &polyShape;
	fixtureDefpoly.density = 1;
	fixtureDefpoly.restitution = 0.3;
	fixtureDefpoly.friction = 0.1;
	b2Fixture* fixture = GroundB2body->CreateFixture(&fixtureDefpoly);
	fixture->SetUserData( (void*)3 );

}
