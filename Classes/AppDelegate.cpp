#include "AppDelegate.h"
#include "Utils.h"
#include <string>
USING_NS_CC;
using namespace std;

typedef struct tagResource
{
    cocos2d::CCSize size;
    char directory[100];
}Resource;

static Resource smallResource  =  { cocos2d::CCSizeMake( 320, 480),   "ipadhd" };
static Resource mediumResource =  { cocos2d::CCSizeMake(640, 960 ),  "ipadhd"   };
static Resource largeResource  =  { cocos2d::CCSizeMake(768, 1280), "ipadhd" };
static cocos2d::CCSize designResolutionSize = cocos2d::CCSizeMake(320, 480);


AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
	// initialize director
	CCDirector* pDirector = CCDirector::sharedDirector();
	CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();

	pDirector->setOpenGLView(pEGLView);
	CCSize frameSize = pEGLView->getFrameSize();
	LOGD("framesize %f, %f", frameSize.width, frameSize.height);

	float scale = frameSize.width/designResolutionSize.width;
	designResolutionSize.height = ceilf(frameSize.height / scale);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
pEGLView->setDesignResolutionSize(designResolutionSize.width,
		designResolutionSize.height, kResolutionShowAll);
#else
pEGLView->setDesignResolutionSize(designResolutionSize.width,
		designResolutionSize.height, kResolutionFixedWidth);
#endif

	vector<string>searchPath;

	// In this demo, we select resource according to the frame's height.
	// If the resource size is different from design resolution size, you need to set contentScaleFactor.
	// We use the ratio of resource's height to the height of design resolution,
	// this can make sure that the resource's height could fit for the height of design resolution.

	// if the frame's height is larger than the height of medium resource size, select large resource.
	if (frameSize.height > mediumResource.size.height)
	{
		LOGD("large resource");
		searchPath.push_back(largeResource.directory);
		pDirector->setContentScaleFactor(largeResource.size.width/designResolutionSize.width);
	}
	// if the frame's height is larger than the height of small resource size, select medium resource.
	else if (frameSize.height > smallResource.size.height)
	{
		searchPath.push_back(mediumResource.directory);
		pDirector->setContentScaleFactor(largeResource.size.width/designResolutionSize.width);
	}
	// if the frame's height is smaller than the height of medium resource size, select small resource.
	else
	{
		searchPath.push_back(smallResource.directory);
		pDirector->setContentScaleFactor(largeResource.size.width/designResolutionSize.width);
	}

	// set searching path
	CCFileUtils::sharedFileUtils()->setSearchPaths(searchPath);

	pDirector->setDisplayStats(false);

	// set FPS. the default value is 1.0/60 if you don't call this
	pDirector->setAnimationInterval(1.0 / 60);

	// create a scene. it's an autorelease object
	CCScene *pScene = MainMenu::scene();

	// run
	pDirector->runWithScene(pScene);

	return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    CCDirector::sharedDirector()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    CCDirector::sharedDirector()->startAnimation();
    //Pause *p = (Pause *) Utils::layerWithTag(TAG_PAUSE);
    //p->pause();
    // if you use SimpleAudioEngine, it must resume here
   //SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
