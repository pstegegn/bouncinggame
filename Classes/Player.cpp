#include "Player.h"

Player::Player(){
	}

Player::~Player(){
	//_playerBody = NULL;
	}

Player * Player::create(){
	Player * player = new Player();
	if (player && player->initWithFile("player.png")) {
		player->initPlayer();
		player->autorelease();
		return player;
	}
	CC_SAFE_DELETE(player);
	return NULL;
}

void Player::initPlayer(){
	//this->scheduleUpdate();
	//this->setAnchorPoint(ccp(0,0));
	this->setPosition(CCPoint(Utils::s().width * 0.2f, Utils::s().height * 0.8f ));
	_angleCount = 0;
	//this->setScaleX(1.4);

}
void Player::createB2Body(){
	//bouncing ball
	b2BodyDef BodyDef;
	BodyDef.type = b2_dynamicBody;
	//BodyDef.fixedRotation = true;
	BodyDef.position.Set(Utils::s().width * 0.2f / PTM_RATIO, Utils::s().height * 0.8f / PTM_RATIO);
	_playerBody = _world->CreateBody(&BodyDef);

	b2PolygonShape playerShape;
	playerShape.SetAsBox(this->getContentSize().width/PTM_RATIO/2, this->getContentSize().height/PTM_RATIO/2,
				 b2Vec2(0,0/*this->getContentSize().width/PTM_RATIO/2, this->getContentSize().height/PTM_RATIO/2*/), 0);

	//define fixture
	b2FixtureDef playerFixDef;
	playerFixDef.shape = &playerShape;//&circle;
	playerFixDef.density = 1;
	playerFixDef.restitution = 0.0;
	playerFixDef.friction = 0.1;
	playerFix = _playerBody->CreateFixture(&playerFixDef);
	playerFix->SetUserData( (void*)1 );
	//_playerBody->SetLinearDamping(0.75);

/*	BodyDef.position.Set(Utils::s().width * 0.2f / PTM_RATIO, Utils::s().height * 0.7f / PTM_RATIO);
	BodyDef.fixedRotation = true;
	//BodyDef.position.Set(Utils::s().width * 0.5f / PTM_RATIO, Utils::s().height * 0.5f / PTM_RATIO);
	_circleBody = _world->CreateBody(&BodyDef);
	b2CircleShape circleShape;
	circleShape.m_radius = this->getContentSize().width/PTM_RATIO/2;
	//define fixture
	playerFixDef.shape = &circleShape;
	playerFixDef.density = 1;
	playerFixDef.restitution = 0.0;
	playerFixDef.friction = 0.1;
	playerFix = _circleBody->CreateFixture(&playerFixDef);
	playerFix->SetUserData( (void*)1 );
	//_playerBody->SetLinearDamping(0.75);


	b2PrismaticJointDef prismaticJointDef;
	prismaticJointDef.bodyA = _playerBody;
	prismaticJointDef.bodyB = _circleBody;
	prismaticJointDef.collideConnected = false;
	prismaticJointDef.localAxisA.Set(0,1);
	prismaticJointDef.localAnchorA.Set( 0,-1*this->getContentSize().width/PTM_RATIO/2);
	prismaticJointDef.localAnchorB.Set(0,this->getContentSize().width/PTM_RATIO/2);
	 prismaticJointDef.enableLimit = true;
	 prismaticJointDef.lowerTranslation = 0;
	prismaticJointDef.upperTranslation = 2;


	_world->CreateJoint( &prismaticJointDef );

	b2DistanceJointDef distanceJointDef;
	distanceJointDef.bodyA = _playerBody;
	distanceJointDef.bodyB = _circleBody;
	distanceJointDef.length = 2;
	distanceJointDef.localAnchorA.Set( 0,-1*this->getContentSize().width/PTM_RATIO/2);
	distanceJointDef.localAnchorB.Set(0,this->getContentSize().width/PTM_RATIO/2);
	distanceJointDef.dampingRatio = 0.1;
	distanceJointDef.frequencyHz = 3;
	_world->CreateJoint( &distanceJointDef );*/

}

void Player::setB2World(b2World* world){
	this->_world = world;
}

void Player::move(){
	float angle = -1*CC_RADIANS_TO_DEGREES(_playerBody->GetAngle());
	//sprite follow the b2body position and rotation
	this->setPosition(CCPoint(_playerBody->GetPosition().x * PTM_RATIO,
			_playerBody->GetPosition().y * PTM_RATIO) );
	this->setRotation (angle);

	//increase gravity
	_playerBody->ApplyForce( _playerBody->GetMass()* 12 *_world->GetGravity(), _playerBody->GetWorldCenter() );
	//_circleBody->ApplyForce( _playerBody->GetMass()* 12 *_world->GetGravity(), _playerBody->GetWorldCenter() );
	//LOGD("angle %f", _playerBody->GetAngularVelocity());
	if(_playerBody->GetAngularVelocity() > 0){
		_playerBody->SetAngularVelocity( -1 );
	}
	if(	(angle > -1   && angle < 1  ) ||
		(angle > 89  && angle < 91 ) ||
		(angle > 179 && angle < 181) ||
		(angle > 269 && angle < 271) ||
		(angle > 359 && angle < 361)
	){
		_playerBody->SetAngularVelocity( 0 );
		//_angleCount -= 1;
	}

	b2Vec2 vel = _playerBody->GetLinearVelocity();
	if(_playerBody->GetPosition().y > Utils::s().height * 1.5f / PTM_RATIO){
			vel.y *= -1;//upwards - don't change x velocity
			_playerBody->SetLinearVelocity( vel );
	}
	if(_playerBody->GetPosition().x < Utils::s().width * 0.2f / PTM_RATIO){
				vel.x = 1;//upwards - don't change x velocity
				_playerBody->SetLinearVelocity( vel );
	}
	if(_playerBody->GetPosition().x > Utils::s().width * 0.2f / PTM_RATIO){
				vel.x= -1;//upwards - don't change x velocity
				_playerBody->SetLinearVelocity( vel );
		}

	prevVel = vel;
}

void Player::jump(){
	b2Vec2 vel = _playerBody->GetLinearVelocity();
	vel.y = 32;//upwards - don't change x velocity
	_playerBody->SetLinearVelocity( vel );

	_playerBody->SetAngularVelocity( 2 );
	//_angleCount += 1;


}

