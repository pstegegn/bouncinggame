#ifndef __Game_H__
#define __Game_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "AppDelegate.h"
#include "Terrain.h"
#include "Ground.h"
#include "Render.h"
#include "Box2D/Box2D.h"
#include "Constants.h"
#include "MainMenu.h"
#include "GameOver.h"
#include "Utils.h"
#include "Pause.h"
#include "Player.h"
#include "HUD.h"
#include "MonsterTruck.h"
#include "jni.h"

#define SHOT_POWER 6.0
#define PTM_RATIO 32
#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,"paulos",__VA_ARGS__)

using namespace cocos2d;
using namespace CocosDenshion;

class Game : public cocos2d::CCLayer
{
public:
	static cocos2d::CCScene* scene();
	~Game();
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();  
    void initializeGame();
    virtual void onEnterTransitionDidFinish();
    virtual void onExit();

    virtual void ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void update(float dt);

    //virtual void draw();
    void submitScore();

    // implement the "static node()" method manually
    CREATE_FUNC(Game);
	Terrain * _terrain;
	CCSpriteBatchNode * _gameBatchNode;
	CC_SYNTHESIZE(b2World *, _world, World);
	Ground *_ground;
	MonsterTruck *_truck;
	CCLabelTTF *tutLabel;
private:
	Player* _player;

    b2Body * body;
    b2Body * kinBody;
    b2CircleShape circle;
    b2PolygonShape square;
	b2FixtureDef fixtureDef1;
	b2Fixture* fixture;
	b2Fixture* kinFixture;

    GLESDebugDraw * m_debugDraw;

    CCArray * _pockets;

    CCSize _screenSize;

    b2Vec2 prevVel;
    CCPoint firstTouch;
    CCPoint lastTouch;
    bool jumping;
    long double time_0;
    long double time_1;
    struct timeval  tv;

    bool pause;
    bool crushed;

};

#endif // __HELLOWORLD_SCENE_H__
