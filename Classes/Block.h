#ifndef __BLOCK_H__
#define __BLOCK_H__
#include "cocos2d.h"
#include "GameSprite.h"
#include "Box2D/Box2D.h"
//#include "Utils.h"

#define TILE_H_SIZE 9
#define TILE_W_SIZE 10
#define PTM_RATIO 32
#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,"paulos",__VA_ARGS__)

USING_NS_CC;

enum  {
    kBlockGap,
    kBlock1,
    kBlock2,
    kBlock3,
    kBlock4
};

class Block : public GameSprite {
    CCSize _screenSize;
    int _tileWidth;
    int _tileHeight;
public:
    CC_SYNTHESIZE(int, _type, Type);
    CC_SYNTHESIZE(b2World* , _world, World);
    CC_SYNTHESIZE(b2Body *, _blockB2body, BlockB2body);

    Block();
    ~Block();
    static Block * create();

    void createB2Body();

    void destroyB2Body();
    void initBlock();
    void setupBlock (int width, int height, float yPositionRatio, int type, b2World* world);
    void setupBlock ();
    inline virtual int left() {
    	return this->getPositionX();
	}

	inline virtual int right() {
    	return this->getPositionX() + _width;
	}

    inline virtual int top() {
        return this->getHeight();
    }

    inline virtual int bottom() {
		return 0;
    }

};

#endif // __BLOCK_H__
