//
//  GameButton.cpp
//  moleit-x
//
//  Created by Todd Perkins on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "GameButton.h"
#include "Utils.h"
//#include <string>

using namespace cocos2d;

bool GameButton::initWithText(const char * text, bool isBig, bool pressed)
{
    if (!CCSprite::init()) {
        return false;
    }

    cocos2d::CCSize s = CCDirector::sharedDirector()->getWinSize();

    if(pressed){
    	this->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("buttonPressed.png"));
    	//this->initWithFile("buttonPressed.png");
    }else{
    	this->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("button.png"));
    	//this->initWithFile("button.png");
    }
	//this->setScale(Utils::getArtScaleFactor());
	//this->setScaleX((Utils::size().width*0.33 )/ this->getContentSize().width);
	//LOGD("btn, %f %f",this->getContentSize().width/Utils::size().width, this->getContentSize().height);

    //CCString* btnFrame = (isBig) ? CCString::create("button_big.png") : CCString::create("button_small.png");
    //int fSize = 16 * Utils::getArtScaleFactor();
    //this->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(btnFrame->getCString()));

    CCLabelTTF *label = CCLabelTTF::create(text, FONTSYLE, TITLE_FONT_SIZE);
    label->setPosition(ccp(this->getContentSize().width/2,this->getContentSize().height/2));
    this->addChild(label,1);
    
    CCLabelTTF *labelShadow = CCLabelTTF::create(text, FONTSYLE, TITLE_FONT_SIZE);
    labelShadow->setPosition(ccp(this->getContentSize().width/2 - (2 + isBig * 2),this->getContentSize().height/2));
    labelShadow->setColor(ccBLACK);
    labelShadow->setOpacity(150);
    this->addChild(labelShadow, 0);
    
   // this->setScale(1.0f);

    return true;
}

bool GameButton::initWithImage(const char * imagePath, bool pressed)
{
    if (!CCSprite::init()) {
        return false;
    }

    cocos2d::CCSize s = CCDirector::sharedDirector()->getWinSize();
    //CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("texture.plist");

    if(pressed){
    	this->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("buttonPressed.png"));
    	//this->initWithFile("buttonPressed.png");
    }else{
    	this->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("button.png"));
    	//this->initWithFile("button.png");
    }
	//this->setScale(2);
	//this->setScaleX((Utils::size().width*0.33 )/ this->getContentSize().width);
	//LOGD("btn, %f %f",this->getContentSize().width/Utils::size().width, this->getContentSize().height);

    //CCString* btnFrame = (isBig) ? CCString::create("button_big.png") : CCString::create("button_small.png");
    //int fSize = 16 * Utils::getArtScaleFactor();
    //this->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(btnFrame->getCString()));

/*
    CCLabelTTF *label = CCLabelTTF::create(imagePath, CCString::createWithFormat("TOONISH.ttf")->getCString(),TITLE_FONT_SIZE);
    label->setPosition(ccp(this->getContentSize().width/2,this->getContentSize().height/2));
*/
    CCSprite * ib = CCSprite::create();
    //ib->initWithFile(imagePath);
    ib->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(imagePath));
    ib->setPosition(ccp(this->getContentSize().width/2,this->getContentSize().height/2));

    this->addChild(ib,1);

   // this->setScale(1.0f);

    return true;
}

GameButton* GameButton::buttonWithText(const char * text, bool isBig, bool pressed)
{
    GameButton *btn = new GameButton();
    btn->initWithText(text, isBig, pressed);
    btn->autorelease();
    return btn;
}

GameButton* GameButton::buttonWithImage(const char * imagePath, bool pressed)
{
    GameButton *btn = new GameButton();
    btn->initWithImage(imagePath, pressed);
    btn->autorelease();
    return btn;
}


