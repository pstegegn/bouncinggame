//
//  Pause.cpp
//  moleit-x
//
//  Created by Todd Perkins on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "Pause.h"
#include "Utils.h"
#include "Game.h"
#include "Constants.h"
#include "GameButton.h"
#include "MainMenu.h"

using namespace cocos2d;

bool Pause::init()
{
    if (!CCLayer::init()) {
        return false;
    }

    bg = CCSprite::create();
    bg->setPosition(ccp(Utils::s().width/2,Utils::s().height/2));
    bg->setTextureRect(CCRectMake(0, 0, Utils::s().width, Utils::s().height));
    bg->setColor(ccBLACK);
    bg->setOpacity(100);
    bg->setVisible(false);
    this->addChild(bg,-1);

    playButton = CCMenuItemSprite::create(
    		GameButton::buttonWithImage("play.png", false),
    		GameButton::buttonWithImage("play.png", true)
    		,this,menu_selector(Pause::resume));
    playButton->setScale(0.7);

    menuPlay = CCMenu::create(playButton, NULL);
    menuPlay->setPosition(ccp(Utils::size().width - playButton->getContentSize().width * (0.5 - 0.15) ,
        		Utils::size().height - playButton->getContentSize().height*(0.5 - 0.15) ));
        this->addChild(menuPlay,1);

    pauseButton = CCMenuItemSprite::create(
    		GameButton::buttonWithImage("pause.png", false),
    		GameButton::buttonWithImage("pause.png", true)
    		,this,menu_selector(Pause::pause));
    pauseButton->setScale(0.7);

    menuPause = CCMenu::create(pauseButton, NULL);
      menuPause->setPosition(ccp(Utils::size().width - pauseButton->getContentSize().width * (0.5 - 0.15) ,
      		Utils::size().height - pauseButton->getContentSize().height*(0.5 - 0.15) ));
      this->addChild(menuPause,1);

    return true;
}

void Pause::pause()
{
	this->togglePause(true);
}

void Pause::resume()
{
	this->togglePause(false);
}

void Pause::mainMenu()
{
	 CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1,MainMenu::scene()));
}

void Pause::togglePause(bool paused)
{
	SimpleAudioEngine::sharedEngine()->playEffect("press.mp3");
	CCScene *g = CCDirector::sharedDirector()->getRunningScene();
    if (paused) {
        g->pauseSchedulerAndActions();
    }
    else {
        g->resumeSchedulerAndActions();
    }
    for (int i = 0; i < g->getChildrenCount(); i++) {
        CCNode *n = (CCNode *)g->getChildren()->objectAtIndex(i);
        if (paused) {
            n->pauseSchedulerAndActions();
        }
        else {
            n->resumeSchedulerAndActions();
        }
    }
    if(paused)
    {
    		SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
    }
    else
    {
        	SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    }

    bg->setVisible(paused);
	((Game*)Utils::gameLayer())->setTouchEnabled(!paused);
	pauseButton->setVisible(!paused);
	playButton->setVisible(paused);

}

void Pause::hidePauseBtn(){
	pauseButton->setVisible(false);
	playButton->setVisible(false);
}




