//
//  Pause.cpp
//  moleit-x
//
//  Created by Todd Perkins on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "GameOver.h"
#include "Game.h"
#include "Constants.h"
#include "GameButton.h"
#include "MainMenu.h"
#include "Utils.h"

using namespace cocos2d;

bool GameOver::init()
{
    if (!CCLayer::init()) {
        return false;
    }

    popup = Popup::popupWithTitle("GAME OVER");
    popup->addButtonWithImage("replay.png", this, menu_selector(GameOver::replay));
    popup->addButtonWithImage("menu_button.png", this, menu_selector(GameOver::mainMenu));
    this->addChild(popup,1);
    
    return true;
}

void GameOver::gameover()
{
	popup->show(true);

	HUD *hud = (HUD *)Utils::layerWithTag(TAG_HUD);

	int s = hud->getScore();

	int hs = CCUserDefault::sharedUserDefault()->getIntegerForKey("no");

	if(hs < s ){
		CCUserDefault::sharedUserDefault()->setIntegerForKey("no", s);
		CCUserDefault::sharedUserDefault()->flush();
	}

    CCString* title = CCString::createWithFormat("Score : %d\nHigh Score : %d", s, hs);

	CCLabelTTF *score = CCLabelTTF::create(title->getCString(), FONTNO, TITLE_FONT_SIZE*0.75);
	score->setPosition(ccp(Utils::s().width *0.5,Utils::s().height / 3 ));
	score->setColor(ccc3(208,254,255));
	this->addChild(score,1);

	if( hs < s){
		CCLabelTTF *newR = CCLabelTTF::create("NEW", FONTNO, TITLE_FONT_SIZE*0.75);
		newR->setPosition(ccp( score->getContentSize().width/2 + Utils::s().width *0.5,
				score->getContentSize().height/2 + Utils::s().height / 3 ));
		newR->setColor(ccRED);
		this->addChild(newR, 1);
	}
}

void GameOver::replay()
{
	SimpleAudioEngine::sharedEngine()->playEffect("press.mp3");
	CCDirector::sharedDirector()->replaceScene(Game::scene());
}

void GameOver::mainMenu()
{
	SimpleAudioEngine::sharedEngine()->playEffect("press.mp3");
	 CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1,MainMenu::scene()));
}




