#include "Game.h"

 jclass cls;
 JNIEnv *myenv;
 JavaVM  *myjvm;

int numFootContacts;
int numFootContacts2;
int gameOverContacts;

class MyContactListener : public b2ContactListener
{
  void BeginContact(b2Contact* contact) {
	  //check if fixture A was the foot sensor
	  void* fixtureUserData = contact->GetFixtureA()->GetUserData();
	  if ( (int)fixtureUserData == 2){
		  numFootContacts++;
	  }
	  //game over
	  if ( (int)fixtureUserData == 3){
		  gameOverContacts++;
	  }

	  //check if fixture B was the foot sensor
	  fixtureUserData = contact->GetFixtureB()->GetUserData();
	  if ( (int)fixtureUserData == 2){
		  //LOGD("--");
		  numFootContacts++;
	  }
	  //game over
	  if ( (int)fixtureUserData == 3){
		  gameOverContacts++;
	  }
  }

  void EndContact(b2Contact* contact) {
	  //check if fixture A was the foot sensor
	  void* fixtureUserData = contact->GetFixtureA()->GetUserData();
	  if ( (int)fixtureUserData == 2)
		  numFootContacts--;
	  //check if fixture B was the foot sensor
	  fixtureUserData = contact->GetFixtureB()->GetUserData();
	  if ( (int)fixtureUserData == 2)
		  numFootContacts--;
  }
};
MyContactListener myContactListenerInstance;

CCScene* Game::scene()
{
    CCScene *sc = CCScene::create();
    sc->setTag(TAG_GAME_SCENE);

    Game *g = Game::create();
    sc->addChild(g, 0, TAG_GAME_LAYER);

    HUD *h = HUD::create();
    sc->addChild(h, 1, TAG_HUD);

    Pause *p = Pause::create();
    sc->addChild(p, 1, TAG_PAUSE);

    GameOver* go  = GameOver::create();
    sc->addChild(go, 1, TAG_GAMEOVER);

    return sc;
}

Game::~Game(){
	delete _world;
	_world = NULL;
	delete m_debugDraw;
}

bool Game::init()
{
	if ( !CCLayer::init())
	{
		return false;
	}
	//SimpleAudioEngine::sharedEngine()->playBackgroundMusic("moles_bg.mp3", true);
	CCSprite *bg = CCSprite::create();
	bg->setPosition(ccp(Utils::s().width/2,Utils::s().height/2));
	bg->setTextureRect(CCRectMake(0, 0, Utils::s().width, Utils::s().height));
	bg->setColor(ccc3(0, 200, 200));
	this->addChild(bg,-1);

	_screenSize = CCDirector::sharedDirector()->getWinSize();
	LOGD("size:w=%f, h=%f", _screenSize.width, _screenSize.height);
	numFootContacts = 0;
	gameOverContacts = 0;
	jumping = false;
	time_0 = 0;
	time_1 = 0;
	pause = false;
	crushed = false;

	this->initializeGame();
	return true;
}

void Game::onEnterTransitionDidFinish()
{
	//LOGD("onEnterTransitionDidFinish...");
	this->setTouchEnabled(true);
	scheduleUpdate();
}

void Game::initializeGame()
{
	b2Vec2 gravity;
	gravity.Set(0.0f, -10.0f);
	_world = new b2World(gravity);
	_world->SetAllowSleeping(true);
	_world->SetContinuousPhysics(true);

/*
	m_debugDraw = new GLESDebugDraw(PTM_RATIO);
	_world->SetDebugDraw(m_debugDraw);
	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;
	//flags += b2Draw::e_jointBit;
	//flags += b2Draw::e_centerOfMassBit;
	//flags += b2Draw::e_aabbBit;
	flags += b2Draw::e_pairBit;
	m_debugDraw->SetFlags(flags);
*/

	_world->SetContactListener(&myContactListenerInstance);

	_gameBatchNode = CCSpriteBatchNode::create("box.png", 200);
	//_gameBatchNode->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("button.png"));
	this->addChild(_gameBatchNode, kMiddleground);

	_terrain = Terrain::create(_world);
	_gameBatchNode->addChild(_terrain, kMiddleground);

	//scrolling ground
	_ground = Ground::create();
	_ground->createB2Body(_world);
	this->addChild(_ground, 1);

    CCSprite *bg = CCSprite::create();
    //bg->initWithFile("landscape.png");
    bg->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("landscape.png"));
    bg->setScaleX(Utils::size().width/bg->getContentSize().width);
    bg->setScaleY(Utils::size().height/bg->getContentSize().height);
    bg->setPosition(ccp(Utils::origin().width + Utils::size().width/2,
    		Utils::origin().height + Utils::size().height/2));
    this->addChild(bg,-1);

    //player
	_truck = MonsterTruck::create();
	this->addChild(_truck);
	_truck->createTruckWithWorld(_world,
			ccp(Utils::size().width*0.2, Utils::size().height*0.52));

	//tutorial
	tutLabel = CCLabelTTF::create("Get Ready!\nTap To Bounce!",
			"",
			Utils::getArtScaleFactor()*24	);
	tutLabel->setPosition(ccp(Utils::origin().width + Utils::size().width/2,
			Utils::origin().height + Utils::size().height*0.7));
	tutLabel->setColor(ccWHITE);
	this->addChild(tutLabel,1);

	//LOGD("cclayer size, %f, %f", this->getContentSize().width, this->getContentSize().height);
}

/*void Game::draw() {
	CCLayer::draw();

	ccGLEnableVertexAttribs(kCCVertexAttribFlag_Position);

	kmGLPushMatrix();

	_world->DrawDebugData();

	kmGLPopMatrix();
}*/

void Game::update(float dt) {
		_world->Step(dt, 8, 1);

		if(gameOverContacts < 1 ){
			_terrain->move(dt * 200);
			_ground->move(dt);
		}

		_truck->updateSprings();
		_truck->move();

		if (jumping) {
			//_player->jump();
			_truck->jump();
			gettimeofday(&tv, NULL);
			time_1 = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000; // convert tv_sec & tv_usec to millisecond
			time_1 -= time_0;

			if (time_1 > 500) {
				time_1 = 0;
				time_0 = 0;
				jumping = false;
			}
		}
		if (gameOverContacts > 0) {
			gettimeofday(&tv, NULL);
			if (time_0 == 0) {
				time_0 = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000; // convert tv_sec & tv_usec to millisecond
			}
			time_1 = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000; // convert tv_sec & tv_usec to millisecond
			time_1 -= time_0;
			if (!crushed) {
				SimpleAudioEngine::sharedEngine()->playEffect("crush.mp3");
				crushed = true;
			}
			this->setTouchEnabled(false);
			_truck->setIsGameOver(true);
			if (time_1 > 700) {
				GameOver *go = (GameOver *) Utils::layerWithTag(TAG_GAMEOVER);
				go->gameover();
				GameOver *gl = (GameOver *) Utils::layerWithTag(TAG_GAME_LAYER);
				gl->setTouchEnabled(false);
				Pause *p = (Pause *) Utils::layerWithTag(TAG_PAUSE);
				p->hidePauseBtn();
				jumping = false;
			}

	}
}

void Game::ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent)
{
	if(!_terrain->getStartTerrain ()){
		_terrain->setStartTerrain ( true );
		_truck->setStart(false);
		tutLabel->setVisible(false);
	}

	if ( numFootContacts < 1 ) return;

	gettimeofday(&tv, NULL);
	time_0 = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000 ; // convert tv_sec & tv_usec to millisecond
	jumping = true;
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("jmp.mp3");

	/*GameOver *go = (GameOver *)Utils::layerWithTag(TAG_GAMEOVER);
	go->gameover();
	this->setTouchEnabled(false);
*/

}

void Game::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event){
	if(jumping){
		jumping = false;
	}
	time_1 = 0;
	time_0 = 0;
}

void Game::onExit()
{
	CCLayer::onExit();
}

void Game::submitScore(){

}
