//
//  MainMenu.cpp
//  moleit-x
//
//  Created by Todd Perkins on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "MainMenu.h"
#include "SimpleAudioEngine.h"
#include "GameButton.h"
#include "Game.h"
#include "Popup.h"
#include "platform/android/jni/JniHelper.h"

using namespace cocos2d;
using namespace CocosDenshion;

//USING_NS_CC;

CCScene* MainMenu::scene()
{
    CCScene *scene = CCScene::create();
    //scene->setTag(7);
    MainMenu *m = (MainMenu*)MainMenu::create();
    scene->addChild(m, 0, 1);

/*
    GameOver* go = (GameOver*)GameOver::create();
    scene->addChild(go, 1, 2);
*/

    return scene;
}

bool MainMenu::init()
{
	if ( !CCLayer::init() )
	{
		return false;
	}
	//Utils::beginSignIn();
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("texture.plist");
	s = CCDirector::sharedDirector()->getWinSize();

	SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	//SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("moles_bg.mp3");
	SimpleAudioEngine::sharedEngine()->preloadEffect("jmp.mp3");
	SimpleAudioEngine::sharedEngine()->preloadEffect("press.mp3");
	SimpleAudioEngine::sharedEngine()->preloadEffect("crush.mp3");
	//LOGD("size %f,%f", s.width, s.height);

    CCSprite *bg = CCSprite::create();
    bg->setPosition(ccp(s.width/2,s.height/2));
    bg->setTextureRect(CCRectMake(0, 0, s.width, s.height));
    bg->setColor(ccc3(100, 200, 200));
    this->addChild(bg,-1);

/*
    CCSprite *play = CCSprite::create();
    //play->setTextureRect(CCRectMake(0, 0, s.width*0.1, s.height*0.1));
    play->initWithFile("button.png");
    play->setPosition(ccp(s.width/2, s.height/2));
    this->addChild(play, 1);
*/


    CCLabelTTF *titleLabel = CCLabelTTF::create("Bouncy Ride",
    		FONTSYLE,
    		TITLE_FONT_SIZE*1.5);
    titleLabel->setPosition(ccp(Utils::size().width/2, Utils::size().height*0.7));
    titleLabel->setColor(ccBLACK);
    this->addChild(titleLabel,1);

    CCLabelTTF *titleShadow = CCLabelTTF::create("Bouncy Ride",
    		FONTSYLE,
			TITLE_FONT_SIZE*1.5);
    titleShadow->setPosition(ccp(Utils::size().width/2 - 0.007*Utils::size().width,
    		Utils::size().height*0.7 + 0.007*Utils::size().height));
    titleShadow->setColor(ccWHITE);
	this->addChild(titleShadow,1);


    //play
    CCMenuItemSprite *playButton = CCMenuItemSprite::create(
    		GameButton::buttonWithImage("play.png", false),
    		GameButton::buttonWithImage("play.png", true),
    		this,
    		menu_selector(MainMenu::playGame));

    //RATE
        CCMenuItemSprite *rateButton = CCMenuItemSprite::create(
        		GameButton::buttonWithText("rate",false, false),
        		GameButton::buttonWithText("rate", false, true),
        		this,
        		menu_selector(MainMenu::rate));
    //board
    CCMenuItemSprite *boardButton = CCMenuItemSprite::create(
    		GameButton::buttonWithImage("leaderboard.png", false),
    		GameButton::buttonWithImage("leaderboard.png", true),
    		this,
    		menu_selector(MainMenu::showLeaderboard));

    CCMenu *menu = CCMenu::create(playButton, rateButton, boardButton, NULL);
    menu->setPosition(ccp(Utils::size().width *0.5, Utils::size().height/3 ));
    //menu->setScale(1.0f);
    this->addChild(menu,1);
    menu->alignItemsHorizontallyWithPadding(playButton->getContentSize().width/4);


	return true;
}

void MainMenu::playGame(CCObject* pSender)
{
	SimpleAudioEngine::sharedEngine()->playEffect("press.mp3");
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1,Game::scene()));
}

void MainMenu::mainMenu(CCObject* pSender)
{
	SimpleAudioEngine::sharedEngine()->playEffect("press.mp3");
    CCDirector::sharedDirector()->replaceScene(MainMenu::scene());
}

void MainMenu::rate(CCObject* pSender)
{
	SimpleAudioEngine::sharedEngine()->playEffect("press.mp3");
	JniMethodInfo t;
	if (JniHelper::getStaticMethodInfo(t, "com.paulos.bouncyride/BouncyRide"
			, "openURL"
			, "()V"))
	{
		t.env->CallStaticVoidMethod(t.classID,t.methodID);
		t.env->DeleteLocalRef(t.classID);
	}
}

void MainMenu::showLeaderboard(CCObject* pSender){
	SimpleAudioEngine::sharedEngine()->playEffect("press.mp3");
	JniMethodInfo t;
	if (JniHelper::getStaticMethodInfo(t, "com.paulos.bouncyride/BouncyRide"
			, "showLeaderboard"
			, "()V"))
	{
		t.env->CallStaticVoidMethod(t.classID,t.methodID);
		t.env->DeleteLocalRef(t.classID);
	}

	HUD *hud = (HUD *)Utils::layerWithTag(TAG_HUD);
	hud->submitScore();

}
