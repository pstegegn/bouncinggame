/*
 * Player.h
 *
 *  Created on: Mar 18, 2014
 *      Author: paulos
 */
#include "Box2D/Box2D.h"
#include "cocos2d.h"
#include "Utils.h"
#include "time.h"

#ifndef PLAYER_H_
#define PLAYER_H_

class Player : public CCSprite{

private:
	b2Fixture* playerFix;
	b2World* _world;
	b2Body * _playerBody;
	b2Body* _circleBody;

	long double time_0;
	long double time_1;
	struct timeval  tv;
	b2Vec2 prevVel;
	int _angleCount;
public:
	Player();
	~Player();
	static Player * create();
	void initPlayer();
	void createB2Body();
	void setB2World(b2World*);
	void move();
	void jump();
};



#endif /* PLAYER_H_ */
