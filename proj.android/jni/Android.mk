LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/Render.cpp \
                   ../../Classes/Game.cpp \
                   ../../Classes/GameSprite.cpp \
                   ../../Classes/Block.cpp \
                   ../../Classes/CCParallaxNodeExtras.cpp \
                   ../../Classes/Ground.cpp \
                   ../../Classes/Terrain.cpp  \
                   ../../Classes/MainMenu.cpp \
                   ../../Classes/GameButton.cpp \
                   ../../Classes/Popup.cpp \
                   ../../Classes/Utils.cpp \
                   ../../Classes/GameOver.cpp \
                   ../../Classes/Pause.cpp \
                   ../../Classes/Player.cpp \
                   ../../Classes/HUD.cpp \
                   ../../Classes/MonsterTruck.cpp
                   
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes \
					$(LOCAL_PATH)/../../../../external/
					
LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
LOCAL_WHOLE_STATIC_LIBRARIES += chipmunk_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static
LOCAL_LDLIBS := -lGLESv1_CM -lstdc++
include $(BUILD_SHARED_LIBRARY)

$(call import-module,cocos2dx)
$(call import-module,cocos2dx/platform/third_party/android/prebuilt/libcurl)
$(call import-module,CocosDenshion/android)
$(call import-module,extensions)
$(call import-module,external/Box2D)
$(call import-module,external/chipmunk)
