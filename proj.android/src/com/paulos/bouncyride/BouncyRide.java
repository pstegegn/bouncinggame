/****************************************************************************
Copyright (c) 2010-2011 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.paulos.bouncyride;

import org.cocos2dx.lib.Cocos2dxGLSurfaceView;
import org.cocos2dx.lib.Cocos2dxRenderer;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ConfigurationInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.*;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.games.Games;

enum state{READY, ONGOING}
enum method{NONE, LEADERBOARD, SUBMITSCORE};

public class BouncyRide extends BaseGameActivity{
	private Cocos2dxGLSurfaceView mGLView;
	private AdView adView;
	private static Context mContext; 
	private static method onMethod = method.NONE;
	private static final String L_ID = "CgkI_OuszLAREAIQAA";
	private static int rand = (int)Math.random() *10000;
	private static int s = 0; 
	static {
	    System.loadLibrary("cocos2dcpp");
	    }  

	@SuppressWarnings("deprecation")
	protected void onCreate(Bundle savedInstanceState) {
		// set requested clients (games and cloud save)
		setRequestedClients(BaseGameActivity.CLIENT_GAMES);

		// enable debug log, if applicable
		if (mDebugLog) {
			enableDebugLog(true, "MyActivity");
		}

		// call BaseGameActivity's onCreate()
		super.onCreate(savedInstanceState);

		if (detectOpenGLES20()) {
			// get the packageName,it's used to set the resource path
			String packageName = getApplication().getPackageName();
			// setPackageName(packageName);
			setContentView(R.layout.main);
			mGLView = (Cocos2dxGLSurfaceView) findViewById(R.id.game_gl_surfaceview);

			mGLView.setEGLContextClientVersion(2);
			mGLView.setCocos2dxRenderer(new Cocos2dxRenderer());

			final TelephonyManager tm = (TelephonyManager) getBaseContext()
					.getSystemService(Context.TELEPHONY_SERVICE);
			String deviceid = tm.getDeviceId();
			Log.d("paulos", "id: " + deviceid);

			adView = (AdView) this.findViewById(R.id.adView);
			adView.setVisibility(AdView.VISIBLE);
			AdRequest adRequest = new AdRequest.Builder()
			.addTestDevice(deviceid).build();
			//.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)

			adView.loadAd(adRequest);
		} else {
			Log.d("activity", "don't support gles2.0");
			finish();
		}
		mContext = BouncyRide.this;
		// beginUserInitiatedSignIn();
	}
  
	public Cocos2dxGLSurfaceView onCreateView() {
		Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
		// SimpleBox2d should create stencil buffer
		glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);
		return glSurfaceView;
	}
  
	private boolean detectOpenGLES20() 
	 {
	     ActivityManager am =
	            (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
	     ConfigurationInfo info = am.getDeviceConfigurationInfo();
	     return (info.reqGlEsVersion >= 0x20000 || Build.FINGERPRINT.startsWith("generic"));
	 }
    
		@Override
	public void onSignInFailed() {
	    Log.d("paulos", "failed ");
	}
	
	@Override
	public void onSignInSucceeded() {
	    // (your code here: update UI, enable functionality that depends on sign in, etc)
	    Log.d("paulos", "successful ");
	    if(onMethod == method.LEADERBOARD){
	    	((BouncyRide)mContext).startActivityForResult(
    				Games.Leaderboards.getLeaderboardIntent(
    						getApiClient(),L_ID), rand);
	    }else if(onMethod == method.SUBMITSCORE){
	    	Games.Leaderboards.submitScore(getApiClient(), 
					L_ID, s);
	    }
	}
	
	public static void submitScore(int s){
		Log.d("paulos", "score "+s);
		
		if(((BouncyRide)mContext).isSignedIn()){
		    // call a Play Games services API method, for example:
			Games.Leaderboards.submitScore(getApiClient(), 
					L_ID, s);
		}
		else {
			//if(stat == state.READY){
				Log.d("paulos", "gameServicesSignIn");
				BouncyRide.onMethod = method.SUBMITSCORE;
				((BouncyRide)mContext).beginUserInitiatedSignIn();
				BouncyRide.s = s;
			//}
		}
	}
	
	public static void gameServicesSignIn() {
		((BouncyRide)mContext).runOnUiThread(new Runnable() {
		public void run() {
				if(!((BouncyRide)mContext).isSignedIn()){
					
						((BouncyRide)mContext).beginUserInitiatedSignIn();
					
				}
			}
		});
	}
	
	public static void showLeaderboard(){
		((BouncyRide)mContext).runOnUiThread(new Runnable() {
	        public void run() {
	        	Log.d("paulos", "showLeaderboard isSignedin " + ((BouncyRide)mContext).isSignedIn());
	        	
	        	if(((BouncyRide)mContext).isSignedIn()){
	        		((BouncyRide)mContext).startActivityForResult(
	        				Games.Leaderboards.getLeaderboardIntent(
	        						getApiClient(),L_ID), rand);
	        	}else{
	        		//if(stat == state.READY){
	    				Log.d("paulos", "gameServicesSignIn");
	    				BouncyRide.onMethod = method.LEADERBOARD;
	    				((BouncyRide)mContext).beginUserInitiatedSignIn();
	        		//}
	        		
	        	}
	        	
	        }
	    });
		
	}

	public static void openURL(){
		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.paulos.bouncyride"));
		((BouncyRide)mContext).startActivity(i);
	}
}
